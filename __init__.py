from math import cos, sin, pi
from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from ctx import Context
import st3m.run

def hsv_to_rgb(h, s, v):
    h *= 6.0
    i = int(h)
    f = h - i   
    p = v * (1.0 - s)
    q = v * (1.0 - s * f)
    t = v * (1.0 - s * (1.0 - f))

    return {
        0: (v, t, p),
        1: (q, v, p),
        2: (p, v, t),
        3: (p, q, v),
        4: (t, p, v)
    }.get(i, (v, p, q))
    
def fibonacci(n):
    sequence = [0, 1]
    while len(sequence) <= n:
        next_value = sequence[len(sequence) - 1] + sequence[len(sequence) - 2]
        sequence.append(next_value)
    return sequence

def draw_circle_with_curves(ctx, x, y, radius):
    k = 0.5522847498
    offset = k * radius
    upper_y = y - radius
    right_x = x + radius
    lower_y = y + radius
    left_x = x - radius

    ctx.move_to(x, upper_y)
    ctx.curve_to(x + offset, upper_y, right_x, y - offset, right_x, y)
    ctx.curve_to(right_x, y + offset, x + offset, lower_y, x, lower_y)
    ctx.curve_to(x - offset, lower_y, left_x, y + offset, left_x, y)
    ctx.curve_to(left_x, y - offset, x - offset, upper_y, x, upper_y)
    
    ctx.fill()


    
class TrippyFlower(Application):

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._angle = 0.
        self._background_hue = 0.
        self._fib_sequence = fibonacci(100)
        self.splitterness = 0
        self.num_recursions = 3
        self.user_feedback_color = (0, 0, 0)  # RGB Color for feedback when a key is pressed


    def draw(self, ctx: Context) -> None:
        r, g, b = hsv_to_rgb(self._background_hue, 1, 1)
        ctx.rgb(r, g, b).rectangle(-120, -120, 240, 240).fill()
        
        # Draw a brief feedback for user input
        if self.user_feedback_color != (0, 0, 0):
            ctx.rgb(*self.user_feedback_color).rectangle(-120, -120, 240, 240).fill()
        
        hue_modulation = (sin(self._angle) + 1) / 2 
        self.draw_flower_fractal(ctx, 0, 0, 60, self._fib_sequence[4+self.splitterness], self.num_recursions, self._angle, hue_modulation)
    


    def draw_flower_fractal(self, ctx, x, y, size, petal_count, depth, angle, hue_modulation):
        if depth == 0 or petal_count >= len(self._fib_sequence):
            return
        petal_length = size
    
        # Modulate hue by depth and the hue_modulation factor to get diverse colors.
        hue_for_circle = (angle / (2 * pi) + depth * 0.1 + hue_modulation) % 1
        r, g, b = hsv_to_rgb(hue_for_circle, 1, 1)
        ctx.rgb(r, g, b)
        draw_circle_with_curves(ctx, x, y, petal_length / 2)
        

        for i in range(petal_count):
            base_angle = (2 * pi / petal_count) * i + angle
            next_base_angle = (2 * pi / petal_count) * (i + 1) + angle

            base_x1 = x + petal_length * cos(base_angle) / 2
            base_y1 = y + petal_length * sin(base_angle) / 2
            base_x2 = x + petal_length * cos(next_base_angle) / 2
            base_y2 = y + petal_length * sin(next_base_angle) / 2

            peak_angle = (base_angle + next_base_angle) / 2
            peak_x = x + petal_length * cos(peak_angle)
            peak_y = y + petal_length * sin(peak_angle)

            # Modulate hue by i (current petal number) to get diverse colors between petals.
            hue_for_petal = (base_angle / (2 * pi) + i * 0.1 + hue_modulation) % 1
            r, g, b = hsv_to_rgb(hue_for_petal, 1, 1)
        
            ctx.rgb(r, g, b)
            ctx.move_to(base_x1, base_y1)
            ctx.curve_to(peak_x, peak_y, peak_x, peak_y, base_x2, base_y2)
            ctx.fill()

            next_angle = angle + (self._angle / (depth * 0.5))
            self.draw_flower_fractal(ctx, peak_x, peak_y, size * 0.6, self._fib_sequence[self._fib_sequence.index(petal_count) + self.splitterness], depth - 1, next_angle, hue_modulation)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        
        direction = ins.buttons.app
        if direction == ins.buttons.PRESSED_LEFT:
            delta_ms *= -1
        elif direction == ins.buttons.PRESSED_RIGHT:
            delta_ms *= 2
        
        # Reset feedback color to default
        self.user_feedback_color = (0, 0, 0)
            
        for i in range(10):
            petal = ins.captouch.petals[i]
            if petal.pressed:
                # Provide feedback for user input
                self.user_feedback_color = (1, 1, 1) if i % 2 == 0 else (0, 0, 0)
                
                if i % 2 == 0:  # Black keys
                    self.splitterness = int(i / 2)
                else:  # White keys
                    self.num_recursions = int((i-1) / 2) + 1
        
        self._angle += min(1, delta_ms / 5000.0) 
        self._background_hue += min(1, delta_ms / 20000.0)
        self._background_hue %= 1.0

if __name__ == '__main__':
    st3m.run.run_view(TrippyFlower(ApplicationContext()))

